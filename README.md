

# lis4381 - Mobile Web Application Development

## Grace Clark

### lis4381 Requirements

*Course Work Links:*

1.  [A1 README.md](a1/README.md "My A1 README.md file")
	* Install Python
	* Install R
	* Install R Studio 
	* Install Visual Studio Code
	* Create *a1_tip_calculator* application
	* Create *a1 tip calculator* Jupyter Notebook
	* Provide screenshots of installations
	* Create Bitbucket repo
	* Complete Bitbucket tutorial (bitbucketstationlocations)
	* Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	* Create Healthy Recipes
	* Provide screenshots of completed apps
	* Provide screenshots of completed Java skill sets

3. [A3 README.md](a3/README.md "My A3 README.md file")
	* Create ERD with MySQL Workbench
	* Add 10 records to each table
	* Provide Screenshots of ERD
	* Create MyEvent Application
	* Provide screenshots of first and second user interface
	* Provid links to a3.mwb and a3.sql

4. [P1 README.md](P1/README.md "My P1 README.md file")
	* Clone student files into lis4381 directory
	* Create an online portfolio web application
	* Modify index.php file
	* Provide screenshot of application and skillsets.

5. [A4 README.md](a4/README.md "My A4 README.md file")
	* Create Bootstrap carousel and local web portfolio using PHP
	* Create a form to collect data using client-side validation
	* Link to local LIS4381 web app
	* Provide screnshots of Skillsets 10-12

6. [A5 README.md](a5/README.md "My A5 README.md file")
	* Review index.php code
	* Add server-side validation and regular expressions
	* Provide skillset screenshots
7. [P2 README.md](P2/README.md "My P2 README.md file")
	* Edit and delete functionality for A5 table
	* RSS Feed