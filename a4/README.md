> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4381

## Grace Clark

### Assignment #4 Requirements:

*Sub-Heading:*

1. Create Bootstrap carousel and local web portfolio using PHP.
2. Create a form to collect data using client-side validation.
3. Link to local LIS4381 web app
4. Skill sets 10-12

#### README.md file should include the following items:

* Screenshot of main page of web app
* Screenshot of passed validation
* Screenshot of failed validation
* Screenshots of Skillsets 10-12


#### Assignment Screenshots:

*Screenshots of MY Online Portfolio*:

| Screenshot of Main Page: | Screenshot of Failed Validation: | Screenshot of Passed Validation: |
|:--------------------------:|:---------------------------:|:---------------------------:|
![](img/OnlinePortfolio.png)  |![](img/SC2.png)  |![](img/client.png)

*Screenshots of Skillsets*:

| Screenshot of Skillset10: | Screenshot of Skillset11: | Screenshot of Skillset12: |
|:--------------------------:|:---------------------------:|:---------------------------:|
![](img/Skillset10.png)  |![](img/Skillset11.png)  |![](img/Skillset12.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
