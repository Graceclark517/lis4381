> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course Title

## Grace Clark

### Project #1 Requirements:

1. Create a launcher icon image and display it in both activities.
2. Add background color to both activities
3. Add border image
4. Add text shadow to button
6. Skill Set 7-9
7. Chapter Questions 7-9

#### README.md file should include the following items:

* Screenshot of application running first user interface
* Screenshot of application running second user interfacer
* Screenshot of skill sets


#### Assignment Screenshots:

*Screenshots of MY Business App*:

| Screenshot of first user interface: | Screenshot of second user interface: |
|:--------------------------:|:---------------------------:|
![](img/Business.png)  |![](img/Business2.png) 

| Screenshot of Skillset7: | Screenshot of Skillset8: | Screenshot of Skillset9: |
|:--------------------------:|:---------------------------:|:---------------------------:|
![](img/Skillset7.png)  |![](img/Skillset8.png)  |![](img/Skillset9.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
