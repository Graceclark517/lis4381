> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4381

## Grace Clark

### Assignment #3 Requirements:

*Sub-Heading:*

1. Create ERD with MySQL Workbench
2. Forward engineer database with 10 records in each table
3. Create event app with launcher icon
4. Add colors to activity controls
5. Add border around image and button
6. Add text shadow (button)

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application's first user interface
* Screenshot of running application's second use interface
* Links to a3.mwb and a3.sql
* Screenshots of skillsets 4, 5, & 6


> #### Assignment Links

* Link to the sql file [sql link](docs/a3.sql) 
* Link to the mwb file [mwb link](docs/mysql.mwb)


#### Assignment Screenshots:
*Screenshot of ERD*:

![ERD Screenshot](img/mysql.png)

*Screenshots of MY Event App*:

| Screenshot of first user interface: | Screenshot of second user interface: |
|:--------------------------:|:---------------------------:|
![](img/Ticket.png)  |![](img/Ticketvalue.png) 

| Screenshot of Skillset4: | Screenshot of Skillset5: | Screenshot of Skillset6: |
|:--------------------------:|:---------------------------:|:---------------------------:|
![](img/Skillset4.png)  |![](img/Skillset5.png)  |![](img/Skillset6.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")