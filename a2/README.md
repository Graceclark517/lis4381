> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4381

## Grace Clark

### Assignment #2 Requirements:

*Sub-Heading:*

1. Healthy Recipe App
2. Chapter Questions
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of running application's first and second user interface
* Screenshot of java skill sets
* git commands with short desriptions
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: creates a new Git repository
2. git commit: changes that will be done on next commit
3. git remote: as a whole uses to exchange changes
4. git push: upload local repository content to a remote repository
5. git pull: merges the retrieved branch heads into the current branch
6. git status: displays the state of the working directory


#### Assignment Screenshots:

*Screenshots of Healthy Recipe*:

| Screenshot of Application: | Screenshot of Recipe:       |
|:--------------------------:|:---------------------------:|
![](img/BruschettaHome.png)  |![](img/BruschettaRecipe.png) 

| Screenshot of Skillset1: | Screenshot of code:       |
|:--------------------------:|:---------------------------:|
![](img/EvenOrOddFinal.png)  |![](img/EvenOrOdd.png) 

| Screenshot of Skillset2: | Screenshot of code:       |
|:--------------------------:|:---------------------------:|
![](img/TwoLargestNumbers.png)  |![](img/LargestNumber.png) 

| Screenshot of Skillset3: | Screenshot of code:       |
|:--------------------------:|:---------------------------:|
![](img/Loop.png)  |![](img/Loops.png) 

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
