> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>
# lis4381- Mobile Web Application Development	

## Grace Clark

### Assignment #5 Requirements:

1. Index.php shows table that displays db information
2. Buttons for edit and delete on table rows
3. Add_petstore page, placeholders describe what must be inserted in input boxes.
4. Add_petstore_process page, processes information inputted on add_petstore, validates information and ensures the necessary boxes were filled. Prompts error if boxes are left blank.
5. Skillsets 13-15
6. Web app link

#### README.md file should include the following items:

* Screenshot of Populated Index table
* Skillsets 13-15
* Web app http://localhost/repos/lis4381/


#### Assignment Screenshots:

*Screenshots of Skillsets*:

| Screenshot of Skillset 13: | Screenshot of Addition: | Screenshot of Results: |
|:--------------------------:|:---------------------------:|:---------------------------:|
![](img/Skillset13.png)  |![](img/Skillsets14.png)  |![](img/Results.png)

*Screenshots of Skillsets*:

| Screenshot of Division: | Screenshot of Result: | Screenshot of File Data: | Screenshot of Results
|:--------------------------:|:---------------------------:|:---------------------------:| :---------------------------:|
![](img/Division.png)  |![](img/result.png)  |![](img/Filedata.png)  |![](img/Read.png)





#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
