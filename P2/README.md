> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Grace Clark

### Project #2 Requirements:

*Sub-Heading:*

1. Edit and Delete functionality added to petstore table
2. Delete gives user a prompt to confirm
3. Edit process page pulls current data of entry
4. RSS feed of student's choice

#### README.md file should include the following items:

* Screenshot of homepage
* Screenshot of index.php
* Screenshot of editing
* Screenshot of editing error
* Screenshot of edited index
* Screenshot of delete prompt
* Screenshot of successful delete
* Screenshot of RSS feed.


#### Assignment Screenshots:

| Screenshot of Homepage: | Screenshot of Index: | Screenshot of Edit Page: 
|:--------------------------:|:---------------------------:|:---------------------------:|
![](img/OnlinePortfolio.png)  |![](img/P2.png)  |![](img/Edit.png)	

![Screenshot of Error](img/Project2.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
